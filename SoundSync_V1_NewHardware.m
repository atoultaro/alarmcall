
% Jul 30, 2015
% Author: Yu Shiu

clear;
% Samplerate
%Fs = 48000;
%SecBetPulse = 60; % the interval between two adjacent pulses
Param.Fs = 48000;
Param.SecBetPulse = 60;

% Sound folder
%SoundMonoFolder = 'C:\ASE_Data\__AlarmCall\__ChannelCombine\dual coincident quad arrays May 6th 2015';
%SoundMonoFolder = 'C:\ASE_Data\__AlarmCall\__ChannelCombine\quad sync drift test May 7th';'
%SoundMonoFile = 'C:\ASE_Data\__AlarmCall\NSFAlarmCalls_48k_gain_test_R-26_0622_145358_1.wav';
%SoundMonoFile = 'P:\ArrayCompaison_20150921\__CablelessArray\Mic1.wav';
SoundMonoFile = 'C:\ASE_Data\__AlarmCall\__201510\NSFAlarmCalls_48k_sample_SoSv2_output_20151020_173830.wav';
OutputFilename = 'SyncMic1_Temp.wav';

[SamplesTarget, Fs] = audioread(SoundMonoFile, 'double');

GPSChan = 1;
%PulseLocList{ii,1} = GetLargePulsePerSeg(SamplesTarget(:,GPSChan), SoundTarget.samplerate);
[PulseLocList, PulseSmallLocList] = GetLargePulsePerSeg_V2(SamplesTarget(:,GPSChan), Fs);

if 0
[SamplesOut, Fs] = TimeAlignStretchSeg(SoundMonoFolder, SoundList, PulseLocList, Param); % create new sound files under a folder in SoundMonoFolder

% Write wav output
audiowrite(fullfile(SoundMonoFolder,OutputFilename), SamplesOut,Fs);
end



