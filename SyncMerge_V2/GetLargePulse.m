
function PulseLocList = GetLargePulse(SoundInputFolder, SoundList, GPSChan, OnsetThre)

SegDuration = 30*60; % 30-min or 1,800 sec 
SegBuffer = 0.15; % 0.15 sec

PulseLocList = cell(size(SoundList,1),1);
for ii = 1:size(SoundList,1)
    disp(fullfile(SoundInputFolder, SoundList(ii,1).name))

    %% TO-DO: divide-and-conquer: 
    % 30-min could be the best duration. 30*60(30 min)*96,000*8(double) = 1.38 GByte
    SoundTargetName = fullfile(SoundInputFolder, SoundList(ii,1).name);
    SoundInfo = audioinfo(SoundTargetName);
    NumSeg = floor(SoundInfo.TotalSamples/SoundInfo.SampleRate/SegDuration);
    
    PulseLocListTemp = [];
    for ss = 1:NumSeg+1
        if(ss == NumSeg+1)
            [SamplesTarget, Fs] = audioread(SoundTargetName, [floor((ss-1)*SegDuration*SoundInfo.SampleRate+1), SoundInfo.TotalSamples], 'double');
            %DEBUG disp([((ss-1)*SegDuration*SoundInfo.SampleRate+1), SoundInfo.TotalSamples]);
        else
            [SamplesTarget, Fs] = audioread(SoundTargetName, [floor((ss-1)*SegDuration*SoundInfo.SampleRate+1), floor((ss*SegDuration+SegBuffer)*SoundInfo.SampleRate)], 'double');
            % DEBUG disp([((ss-1)*SegDuration*SoundInfo.SampleRate+1), (ss*SegDuration+SegBuffer)*SoundInfo.SampleRate]);        
        end
        SamplesTarget = SamplesTarget(:,GPSChan); % remove the non-gps / sound recording channel
        
        % TO-DO: check col 1 & 2 for large pulse indices
        LargePulseTemp = GetLargePulsePerSeg(SamplesTarget, Fs, OnsetThre);
        LargePulseTemp(:,end) = LargePulseTemp(:,end) + (ss-1)*SegDuration*SoundInfo.SampleRate;
        PulseLocListTemp = [PulseLocListTemp; LargePulseTemp];
        clear SamplesTarget;
    end
    
    % Check duplicates and remove them
    LargePulseTime = PulseLocListTemp(:,1)*60+PulseLocListTemp(:,2);
    LargePulseDuplicate = find(diff(LargePulseTime)==0);
    % remove the row
    if(isempty(LargePulseDuplicate))
        PulseLocListTempNew = PulseLocListTemp;
    else
        PulseLocListTempNew = [];
        for rr = 1:size(PulseLocListTemp,1)
            if(any((LargePulseDuplicate-rr)~=0))
                PulseLocListTempNew = [PulseLocListTempNew; PulseLocListTemp(rr,:)];
            end
        end
    end
    
    % diff(LargePulseTime) should always be 1.0 
    if(any(diff(LargePulseTime) ~= 1))
        if(any(diff(LargePulseTime) == 0))
            disp('ERROR: Large pulses duplicated.')
        elseif(any(diff(LargePulseTime)>=2))
            disp('ERROR: Large pulses missed / not detected.') % TO-DO: merging using sparse large pulse indices
        elseif(any(diff(LargePulseTime)< 0))
            disp('ERROR: Large pulses messed up.')
        end
        return;
    end

    %PulseLocList{ii,1} = PulseLocListTemp;
    PulseLocList{ii,1} = PulseLocListTempNew;
    
    if 0
    [SamplesTarget, Fs] = audioread(fullfile(SoundInputFolder, SoundList(ii,1).name), 'double');
    SamplesTarget = SamplesTarget(:,GPSChan); % remove the non-gps / sound recording channel
    PulseLocList{ii,1} = GetLargePulsePerSeg(SamplesTarget, Fs, OnsetThre);
    clear SamplesTarget;
    end
    
    disp('')
end

%return PulseLocList;