
function PulseLocList = GetLargePulse(SoundMonoFolder, SoundList)

% Dummy
%Samples = 0;
%Fs = 0;

%GPSChan = 1;
GPSChan = 2;
OnsetThre = 0.6;

PulseLocList = cell(size(SoundList,1),1);
for ii = 1:size(SoundList,1)
    disp(fullfile(SoundMonoFolder, SoundList(ii,1).name))
    [SamplesTarget, Fs] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), 'double');
    disp(Fs)
    
    PulseLocList{ii,1} = GetLargePulsePerSeg(SamplesTarget(:,GPSChan), Fs, OnsetThre);
        
end

%return PulseLocList;