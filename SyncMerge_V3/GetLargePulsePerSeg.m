function PulseLocListPerSeg = GetLargePulsePerSeg(Samples, Fs, OnsetThre)
%function AlarmGPS_V1_Func(SoundFile)

%% NSF Alarm Call Project
% Get the large pulse per segment per sound file

% V1: automatic paging
% Apr 23, 2015

FlagPlot = 0;
LargePulseDur = 60.0;

%Samples = double(Samples);
%Onset = [(diff(Samples)>0);0].*(Samples>0).*Samples;
Onset = [0;(diff(Samples)>0)].*(Samples>0).*Samples;
if FlagPlot
    figure(2); plot((1:size(Samples,1))/Fs,Onset); grid; title('Onset');
end
OnsetOn = find(Onset>OnsetThre);

% Find the exact pulse for OnsetOn: "PulseExact"
PulseExact = [];
PulseCurr = -100*Fs; % unit: samples
PulseLargeThre = (LargePulseDur*0.90)*Fs; 
for ii = 1:size(OnsetOn,1)
   if( OnsetOn(ii,1)-PulseCurr > PulseLargeThre )
       PulseCurr = OnsetOn(ii,1);
       PulseExact = [PulseExact; PulseCurr];
   end
end

% One data train
SigThre = 0.1;
%DataLoad = 8; % bytes
DataLoad = 10; % bytes
PulseLocListPerSeg = zeros(size(PulseExact,1),DataLoad+1); % The one more data on DataLoad is the sample number of large pulses
PulseExactCount = 0;
for jj = 1:size(PulseExact,1)
    %Samples2 = Samples(PulseExact(jj,1)+0.150*Fs:PulseExact(jj,1)+0.250*Fs,1); % between 150 msec and 250 sec after the sync pulse
    Samples2 = Samples(PulseExact(jj,1)+0.000*Fs:PulseExact(jj,1)+0.15*Fs,1); % between 0 msec and 150 msec after the sync pulse
    Sample2Diff = diff(Samples2);

    % Find zero-crossing point
    SamplesZC = Samples2(1:end-1,1).*Samples2(2:end,1);
    %%SamplesZC2 = (SamplesZC<0).*(abs(diff(Samples2))>0.05).*sign(diff(Samples2)); % 0.1 is the threshold for level change
    SamplesZC2 = (SamplesZC<=0).*(abs(diff(Samples2))>1e-5).*((sign(diff(Samples2))>0)-0.5)*2; % 0. is the threshold for level change

    % Find the double-pulses
    SyncPulse = [];
    SamplesZCPulse = find(abs(SamplesZC2) == 1);
    for nn = 1:size(SamplesZCPulse)-3+1
        if( SamplesZC2(SamplesZCPulse(nn,1),1)==-1 &&  SamplesZC2(SamplesZCPulse(nn+1,1),1)==1 && SamplesZC2(SamplesZCPulse(nn+2,1),1)==-1 && SamplesZCPulse(nn+2,1)-SamplesZCPulse(nn,1)<1e-3*Fs)
            SyncPulse = [SyncPulse; SamplesZCPulse(nn+2,1)];
        end
    end
    if(length(SyncPulse) ~= DataLoad+1) % there are (DataLoad+1) boundary calls
        disp('ERROR: the message contains message of incorrect length.');
        continue
    end

    MessInBit = zeros(DataLoad,8); % 1 byte = 8 bits
    BitDur = 1e-3*Fs; % sample length for each bit
    for kk = 1:DataLoad
        for ll = 1:8
            %MessInBit(kk,ll) = mode(sign(Samples2( SyncPulse(kk,1)+BitDur*(ll-1)+1+BitDur/4:SyncPulse(kk,1)+BitDur*ll+BitDur/4 ,1)));
            MessInBit(kk,ll) = mode(sign(Samples2( SyncPulse(kk,1)+round(BitDur*(ll-1)+1+BitDur/4):SyncPulse(kk,1)+round(BitDur*ll+BitDur/4) ,1)));
        end
    end
    MessInBit = MessInBit/2.0+0.5;
    for kk = 1:DataLoad
        MessTemp = 0;
        for ll = 1:8
            MessTemp = MessTemp+ MessInBit(kk,ll)*2^(8-ll);
            PulseLocListPerSeg(PulseExactCount+jj,kk) = MessTemp;
        end
    end
    PulseLocListPerSeg(PulseExactCount+jj,DataLoad+1) = PulseExact(jj,1); % write the index number of pulses into an extra column
    disp(PulseLocListPerSeg(PulseExactCount+jj,:));
end
PulseExactCount = PulseExactCount + size(PulseExact,1);

%return PulseLocListPerSeg




















