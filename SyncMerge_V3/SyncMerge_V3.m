% Sunc-and-merge program
% Version 3
% Feb, 2016

% Only support wav, not aiff

% Rule of thumbs usage:
% 10 channels at 48kHz sampling rate might take ? GByte


% Jul 30, 2015
% Author: Yu Shiu

clear;
% Samplerate
%Fs = 48000;
%SecBetPulse = 60; % the interval between two adjacent pulses
Param.Fs = 48000;
Param.SecBetPulse = 60;

% Sound folder
%SoundMonoFolder = 'P:\Array Experiment Example Files Diamond STJA LO 1 13 Jan 2016\__Original Files';
%SoundOutFolder = 'P:\Array Experiment Example Files Diamond STJA LO 1 13 Jan 2016';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SoundMonoFolder = '\\ag-clo-nas.ad.cornell.edu\AlarmCall\Seet 2 all microphones';  %%%<<<<<<======
SoundOutFolder = '\\ag-clo-nas.ad.cornell.edu\AlarmCall\Seet 2 all microphones\__Temp'; % <<<<<<======
FileNameHead = 'SyncAndMerge'; % Needs change this: what is the naming convention? Could be part of UI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%OutputFilename = 'SyncAllChan_Output.wav';

SoundList0 = dir(fullfile(SoundMonoFolder, '*.wav'));
MicIndList = zeros(1, size(SoundList0, 1));
for ff = 1:size(SoundList0, 1)
    [Mat,~] = regexp(SoundList0(ff).name, 'Mic\s(\d+)', 'tokens');
    MicIndList(1, ff) = str2double(Mat{1,1});
end
[MicIndListSort, IndMat] = sort(MicIndList);
SoundList(1,1) = SoundList0(1,1);
for ff = 1:size(SoundList0, 1)
    SoundList(ff,1) = SoundList0(IndMat(1,ff),1);
end

%%SoundList = dir(fullfile(SoundMonoFolder, '*.wav'));
% Get the data payload of large pulses and its sample number / timing
%PulseLocList = GetLargePulse(SoundMonoFolder, SoundList);
PulseLocList = GetLargePulse(SoundMonoFolder, SoundList);

% time sliding calculation
% time stretching calculation
% Generate the synced & stretched sounds
%%[SamplesOut, Fs, BitsperSample] = TimeAlignStretchSeg(SoundMonoFolder, SoundList, PulseLocList, Param); % create new sound files under a folder in SoundMonoFolder

%SegLen = 15; % 15 min
SegLen = 5; % 5 min
%[SamplesOut, Fs, BitsperSample] = TimeAlignStretchSeg(SoundMonoFolder, SoundList, PulseLocList, Param, NameHead, SegLen);
TimeAlignStretchSeg(SoundMonoFolder, SoundOutFolder, SoundList, PulseLocList, Param, FileNameHead, SegLen);

%% Combine segment files into 1 big file, or several big files, such as
% 15-min.
% Need help from Sox. The path to Sox needs to be added in control panel
CurrPath = pwd;
cd(SoundOutFolder);
SoundList1 = dir(fullfile(SoundOutFolder, '*.wav'));
% Target is to combine files in 15-min file format
% Sox Syntax of concatenating files: sox File1.wav File2.wav FileCombined.aif

%% Need to have a formula adjusting the parameters, such as SegLen (time duration for each segment file), based on the memory the computer has.













%% Farm system, or dog house

%% Write wav output
%audiowrite(fullfile(SoundMonoFolder,OutputFilename), SamplesOut,Fs);
% Moved to inside the TimeAlignStretchSeg
%% Write wav output in 15-min segments
if 0
NameHead = 'SyncAndMerge';
SoundLength = size(SamplesOut,1);
SegLen = 15; % 15 min

NumSeg = floor(SoundLength/(Fs*SegLen));
if(BitsperSample == 24)
    BitsperSample = 32;
end
for tt = 1:NumSeg
    SamplesOutSeg = double(SamplesOut((tt-1)*SegLen*Fs+1:tt*SegLen*Fs,:))/(2^(BitsperSample-1));
    audiowrite(fullfile(SoundMonoFolder,[NameHead,int2str(tt),'.wav']), SamplesOutSeg, Fs);
end
end

%% Write out the Pulse Info (PulseLocList)

%% sort based on mic #: problem (1), skipped number; (2) use 1, 2, 3 instead 01, 02, 03. Hard to sort.
if 0
SoundList0 = dir(fullfile(SoundMonoFolder, '*.wav'));
% sort SoundList by the Mic #
% Input: SoundList0; output: SoundList
SoundList = SoundList0;
for ff = 1:size(SoundList0, 1)
    [Mat,~] = regexp(SoundList0(ff).name, 'Mic\s(\d+)', 'tokens');
    MicInd = str2double(Mat{1,1});
    SoundList(MicInd,1) = SoundList0(ff,1);
end
end

