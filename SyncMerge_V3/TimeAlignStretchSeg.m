%function [SamplesOut, Fs, BitsPerSample] = TimeAlignStretchSeg(SoundOutFolder, SoundList, PulseLocList, Param)
function TimeAlignStretchSeg(SoundMonoFolder, SoundOutFolder, SoundList, PulseLocList, Param, NameHead, SegLen)

% Time alignment 
% Time Stretching
% Using all the time points

ChanData = 1; % Channel 1 is the data channel whereas Channel 2 is the sync channel

NumSoundFile = size(SoundList,1);

% Start time: T0; End time: T1
AbsoTime = cell(NumSoundFile,1);
T0 = zeros(NumSoundFile,1);
T1 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    %AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60 + PulseLocList{ii,1}(:,3);
    %T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60 + PulseLocList{ii,1}(1,3);
    %T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60 + PulseLocList{ii,1}(end,3);
    AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60;
    T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60;
    T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60;
end


if 0
[MaxT0, MaxT0Ind] = max(T0);
SyncInstT0 = zeros(NumSoundFile,1);
IndT0 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    IndT0(ii,1) = find(abs(AbsoTime{ii,1}-MaxT0)==0);
    if(ii ~= MaxT0Ind)
        try
            %SyncInstT0(ii,1) = PulseLocList{ii,1}(IndT0(ii,1),9);
            SyncInstT0(ii,1) = PulseLocList{ii,1}(IndT0(ii,1),11); % Synchronizer hardware must've been changed between late November and Feb 3.
        catch
            disp('');
        end
   else
       %SyncInstT0(ii,1) = PulseLocList{ii,1}(1,9);
       SyncInstT0(ii,1) = PulseLocList{ii,1}(1,11);
   end
end

% End time
%[fid,msg] = fopen(fullfile(SoundOutFolder, SoundList(ii,1).name),'rb','l');
[MinT1, MinT1Ind] = min(T1);

PulseNum = (MinT1 - MaxT0)/Param.SecBetPulse;
end


[MinT0, MinT0Ind] = min(T0);
[MaxT1, ~] = max(T1);
[MaxT0, MaxT0Ind] = max(T0);

PulseNum = (MaxT1 - MinT0)/Param.SecBetPulse;

PulseExistTable = zeros(PulseNum, NumSoundFile);
PulseTable = zeros(PulseNum+1, NumSoundFile); % 1 means sound data exist
PulseDelayTable = zeros(PulseNum, NumSoundFile);
IndT0 = zeros(NumSoundFile,1);

for ii = 1:NumSoundFile
    for jj = 1:size(PulseLocList{ii,1},1)-1
        TimeCurrInd = (PulseLocList{ii,1}(jj,1)*3600+PulseLocList{ii,1}(jj,2)*60-MinT0)/60+1;
        PulseExistTable(TimeCurrInd, ii) = 1;
        PulseTable(TimeCurrInd, ii) = PulseLocList{ii,1}(jj,11); % table for sample index
        PulseDelayTable(TimeCurrInd, ii) = Param.Fs*Param.SecBetPulse - (PulseLocList{ii,1}(jj+1,11) - PulseLocList{ii,1}(jj,11)); % table for sample delay 
    end
    TimeCurrInd = (PulseLocList{ii,1}(size(PulseLocList{ii,1},1),1)*3600+PulseLocList{ii,1}(size(PulseLocList{ii,1},1),2)*60-MinT0)/60+1;
    PulseTable(TimeCurrInd, ii) = PulseLocList{ii,1}(size(PulseLocList{ii,1},1),11); 
end

% for ii = 1:NumSoundFile
%     IndT0(ii,1) = find(abs(AbsoTime{ii,1}-MaxT0)==0);
%     for jj = 1:PulseNum
%         PulseTable(jj, ii) = PulseLocList{ii,1}(IndT0(ii,1)+jj-1,11); % IndT0(ii,1)
%     end
% end

% Check BitsPerSample for every file
BitsPerSampleInfo = zeros(1, NumSoundFile);
for ii = 1:NumSoundFile
    SoundInfo = audioinfo(fullfile(SoundMonoFolder, SoundList(ii,1).name));
    BitsPerSampleInfo(1, ii) = SoundInfo.BitsPerSample;
end
BitsPerSample0 = unique(BitsPerSampleInfo);
if (length(BitsPerSample0)~=1)
    disp('ERROR: the input channels have different sample-bit, eg, 24-bit and 32-bit. The program requires that they have the same single sample-bit.')
    %SamplesOut = [];
    return;
end



%% Write out SamplesOut segment every 15-min
% NameHead, SegLen

PulseCount = 0; % 1 pulse every 1-min
SegNum = 0; % output file count
for jj = 1:PulseNum
    fprintf('%d of %d 1-min segments\n', jj, PulseNum);
    % Initialize the array SamplesOutSeg every SegLen min
    if(mod(PulseCount, SegLen)==0)
        if(BitsPerSample0 == 16)
            BitsPerSample = 16;
            SamplesOutSeg = zeros(SegLen*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int16');
        elseif(BitsPerSample0 ==24)
            BitsPerSample = 32;
            SamplesOutSeg = zeros(SegLen*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int32');
        elseif(BitsPerSample0 ==32)
            BitsPerSample = 32;
            SamplesOutSeg = zeros(SegLen*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int32');
        end
    end
    
    s1 = (jj-1-SegLen*floor((jj-1)/SegLen))*Param.Fs*Param.SecBetPulse+1;
    s2 = (jj-SegLen*floor((jj-1)/SegLen))*Param.Fs*Param.SecBetPulse;
    for ii = 1:NumSoundFile
        if(PulseExistTable(jj, ii) == 1) % has pulse, have delay
            %[SamplesBothChanTemp, Fs] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)]);
            %[SamplesBothChanTemp, ~] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'int32'); % Problem!!
            [SamplesBothChanTemp, ~] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)-1], 'native'); % Problem!!
            
            SamplesInTemp = SamplesBothChanTemp(:,ChanData); % Channel 1 is the data channel whereas Channel 2 is the sync channel
            clear SamplesBothChanTemp; 
            if(PulseDelayTable(jj,ii) > 0)
                % insert samples
                SamplesOutTemp = SamplesInsert(SamplesInTemp, PulseDelayTable(jj,ii));
            elseif(PulseDelayTable(jj,ii) <0)
                % drop samples
                SamplesOutTemp = SamplesRemove(SamplesInTemp, -1*PulseDelayTable(jj,ii));
            else % PulseDelayTable(jj,ii) == 0
                % do nothing and out the original samples
                SamplesOutTemp = SamplesInTemp;
            end
        else % PulseExistTable(jj, ii) == 0 % no pulse
            SamplesOutTemp = zeros(Param.Fs*Param.SecBetPulse,1, 'int32');
        end
        %SamplesOut((jj-1)*Param.Fs*Param.SecBetPulse+1:jj*Param.Fs*Param.SecBetPulse,ii) = SamplesOutTemp;
        %SamplesOutSeg((jj-1)*Param.Fs*Param.SecBetPulse+1:jj*Param.Fs*Param.SecBetPulse,ii) = SamplesOutTemp;

        SamplesOutSeg(s1:s2,ii) = SamplesOutTemp;
    end
    
    % Write out the array SamplesOutSeg every SegLen min
    if(mod(PulseCount, SegLen)==SegLen-1)
        SamplesOutSeg = double(SamplesOutSeg)/(2^(BitsPerSample-1));
        
        %SegNum = floor(PulseCount/SegLen)+1;
        % Use SegNum as a count
        SegNum = SegNum +1;
        if(SegNum < 10)
            audiowrite(fullfile(SoundOutFolder,[NameHead,'_000',int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs, 'BitsPerSample',BitsPerSample0); 
        elseif(SegNum < 100)
            audiowrite(fullfile(SoundOutFolder,[NameHead,'_00',int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs, 'BitsPerSample',BitsPerSample0); 
        elseif(SegNum < 1000)
            audiowrite(fullfile(SoundOutFolder,[NameHead, '_0', int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs, 'BitsPerSample',BitsPerSample0); 
        elseif(SegNum < 10000)
            audiowrite(fullfile(SoundOutFolder,[NameHead, int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs); 
        else
            disp('ERROR: This program currently only supports up to 9,999 segments / files.')
        end
    end
    PulseCount = PulseCount + 1;
end
% Write out the remaining part of sounds less than SegLen
if(mod(PulseCount-1, SegLen)~=SegLen-1)
    SamplesOutSeg = double(SamplesOutSeg)/(2^(BitsPerSample-1));
    SegNum = SegNum +1;
    if(SegNum < 10)
        audiowrite(fullfile(SoundOutFolder,[NameHead,'_000',int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs, 'BitsPerSample',BitsPerSample0); 
    elseif(SegNum < 100)
        audiowrite(fullfile(SoundOutFolder,[NameHead,'_00',int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs, 'BitsPerSample',BitsPerSample0); 
    elseif(SegNum < 1000)
        audiowrite(fullfile(SoundOutFolder,[NameHead, '_0', int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs, 'BitsPerSample',BitsPerSample0); 
    elseif(SegNum < 10000)
        audiowrite(fullfile(SoundOutFolder,[NameHead, int2str(SegNum),'.wav']), SamplesOutSeg, Param.Fs); 
    else
        disp('ERROR: This program currently only supports up to 9,999 segments / files.')
    end
end



















if 0
%SamplesOut = zeros(PulseNum*Param.Fs*Param.SecBetPulse ,NumSoundFile);
if(BitsPerSample == 16)
    SamplesOut = zeros(PulseNum*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int16');
elseif(BitsPerSample ==24)
    SamplesOut = zeros(PulseNum*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int32');
elseif(BitsPerSample ==32)
    SamplesOut = zeros(PulseNum*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int32');
end

for ii = 1:NumSoundFile
    fprintf('%d of %d sound files\n', ii, NumSoundFile);
    for jj = 1:PulseNum
        if(PulseExistTable(jj, ii) == 1) % has pulse, have delay
            %[SamplesBothChanTemp, Fs] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)]);
            %[SamplesBothChanTemp, ~] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'int32'); % Problem!!
            [SamplesBothChanTemp, ~] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)-1], 'native'); % Problem!!
            
            % debug
            if(ii == 4 && jj==10)
                disp('');
            end
            
            SamplesInTemp = SamplesBothChanTemp(:,ChanData); % Channel 1 is the data channel whereas Channel 2 is the sync channel
            clear SamplesBothChanTemp; 
            if(PulseDelayTable(jj,ii) > 0)
                % insert samples
                SamplesOutTemp = SamplesInsert(SamplesInTemp, PulseDelayTable(jj,ii));
            elseif(PulseDelayTable(jj,ii) <0)
                % drop samples
                SamplesOutTemp = SamplesRemove(SamplesInTemp, -1*PulseDelayTable(jj,ii));
            else % PulseDelayTable(jj,ii) == 0
                % do nothing and out the original samples
                SamplesOutTemp = SamplesInTemp;
            end
        else % PulseExistTable(jj, ii) == 0 % no pulse
            SamplesOutTemp = zeros(Param.Fs*Param.SecBetPulse,1, 'int32');
        end
        SamplesOut((jj-1)*Param.Fs*Param.SecBetPulse+1:jj*Param.Fs*Param.SecBetPulse,ii) = SamplesOutTemp;
    end
end
Fs = Param.Fs;
end























if 0
IndT0 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    IndT0(ii,1) = find(abs(AbsoTime{ii,1}-MinT0)==0);
end
PulseNum = (MaxT1 - MinT0)/Param.SecBetPulse;
PulseTable = zeros(PulseNum, NumSoundFile);
for ii = 1:NumSoundFile
    for jj = 1:PulseNum
        PulseTable(jj, ii) = PulseLocList{ii,1}(IndT0(ii,1)+jj-1,11); % IndT0(ii,1)
    end
end

SampleDropped = Param.Fs*Param.SecBetPulse - (diff(PulseTable) + 1); % they were dropped during recording process and hence should be added



%SamplesOut = zeros((PulseNum-1)*Param.Fs*Param.SecBetPulse ,NumSoundFile,'int16');
SamplesOut = zeros((PulseNum-1)*Param.Fs*Param.SecBetPulse ,NumSoundFile, 'int32');
for ii = 1:NumSoundFile
    NumSeg = size(SampleDropped,1);
    for jj = 1:NumSeg
        %SamplesOutTemp = [];
        fprintf('%d of %d\n', jj, NumSeg);
        %[SamplesBothChanTemp, Fs] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'native');
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'int32');
        %[SamplesBothChanTemp, Fs] = audioread(fullfile(SoundOutFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)]);
        SamplesInTemp = SamplesBothChanTemp(:,1); % Channel 1 is the data channel whereas Channel 2 is the sync channel
        clear SamplesBothChanTemp; 
        
        
        if(SampleDropped(jj,ii) > 0)
            % insert samples
            SamplesOutTemp = SamplesInsert(SamplesInTemp, SampleDropped(jj,ii));
        elseif(SampleDropped(jj,ii) <0)
            % drop samples
            SamplesOutTemp = SamplesRemove(SamplesInTemp, -1*SampleDropped(jj,ii));
        else % SampleDropped(jj,ii) == 0
            % do nothing and out the original samples
            SamplesOutTemp = SamplesInTemp
        end
        
        try
            SamplesOut((jj-1)*Param.Fs*Param.SecBetPulse+1:jj*Param.Fs*Param.SecBetPulse,ii) = SamplesOutTemp;
        catch
            disp();
        end
    end
    disp('');
end
end




function SamplesOutput = SamplesInsert(SamplesInput, SamplesToPut)
SamplesOutput = [];
% How to insert samples?
% * uniform samples the indice and record them
% * read over all samples and insert accordingly
%SamplesInsertLen = floor(Param.Fs*Param.SecBetPulse/(SamplesToPut+1)); %% <<== problem!!
SamplesInsertLen = floor(length(SamplesInput)/(SamplesToPut+1));
if((ceil(length(SamplesInput)/SamplesInsertLen)-1)-1 ~= SamplesToPut)
    fprintf('');
end

for pp = 1:SamplesToPut
    SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1); SamplesInput(pp*SamplesInsertLen,1)];
end
pp = pp+1;
SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesInsertLen+1:end,1)];


function SamplesOutput = SamplesRemove(SamplesInput, SamplesToGet)
SamplesOutput = [];
SamplesRemoveLen = floor(length(SamplesInput)/(SamplesToGet+1));

for pp = 1:SamplesToGet
    %SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1); SamplesInput(pp*SamplesInsertLen,1)];
    SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesRemoveLen+1:pp*SamplesRemoveLen-1,1)]; % remove one sample at the end of each segment
end
pp = pp+1;
SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesRemoveLen+1:end,1)];
















