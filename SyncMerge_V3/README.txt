Find the longest length that include all recorded sound data. When sound data are missing, zero-samples are added. 
This is the opposite to the "longest common length". This is OR operation whereas the latter is AND operation.
