% Calculate the impact of height on localization
% 3D or 2D localization

SoundSpeed = 331.3; % sound speed at 0 deg C

%% Fixed height, fixed bearing, fixed radius
if 0
% No 3D; height = 0;
Theta = pi/4; 
DRadius = 20; % radius of sound
MRadius=10; % radius of microphone
% Original time difference
TimeDiff = sqrt((MRadius+DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2)-sqrt((MRadius-DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2);

% With height H
HH = 0:10;
TimeDiff3D = sqrt((MRadius+DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2+HH.^2)-sqrt((MRadius-DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2+HH.^2);

% time diff for the height H
TimeDiffHH = TimeDiff3D(1,end);

% inverse process: having time delay, assume 2D, find the location (incorrect one)
tt = 0:0.1:90; % 901 points
Theta1 = pi*(tt/180);
TimeDiff2D = sqrt((MRadius+DRadius*cos(Theta1)).^2+(DRadius*sin(Theta1)).^2)-sqrt((MRadius-DRadius*cos(Theta1)).^2+(DRadius*sin(Theta1)).^2);

[ValDeg, IndDeg] = min(abs(TimeDiff2D-TimeDiffHH));
DegMistaken = (IndDeg-1)*0.1;
ThetaMistaken = DegMistaken/180*pi; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Location difference
% 50 deg vs 45 deg
% As DRadius = 20m, 45deg ==> 55.5 deg
%Psi1 = 50.0/180*pi;
%Psi2 = 45/180*pi;
DiffLoc = DRadius*sqrt(((sin(Theta)-sin(ThetaMistaken))^2) + ((cos(Theta)-cos(ThetaMistaken))^2) );
end


%% Varying height, fixed bearing (pi/4), fixed radius (20 m)
if 0
% No 3D; height = 0;
Theta = pi/4; 
DRadius = 20; % radius of sound
MRadius=10; % radius of microphone
% Original time difference
TimeDiff = sqrt((MRadius+DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2)-sqrt((MRadius-DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2);

HH = 0:10;
TimeDiff3D = sqrt((MRadius+DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2+HH.^2)-sqrt((MRadius-DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2+HH.^2);
% time diff for the height H
%TimeDiffHH = TimeDiff3D(1,end); <<== make it vary

% inverse process: having time delay, assume 2D, find the location (incorrect one)
tt = 0:0.1:90; % 901 points
Theta1 = pi*(tt/180);
TimeDiff2D = sqrt((MRadius+DRadius*cos(Theta1)).^2+(DRadius*sin(Theta1)).^2)-sqrt((MRadius-DRadius*cos(Theta1)).^2+(DRadius*sin(Theta1)).^2);

DiffLoc = zeros(1, length(TimeDiff3D));
for tt = 1:length(TimeDiff3D)
    [ValDeg, IndDeg] = min(abs(TimeDiff2D-TimeDiff3D(1,tt)));
    DegMistaken = (IndDeg-1)*0.1;
    ThetaMistaken = DegMistaken/180*pi; 
    % Location difference
    DiffLoc(1,tt) = DRadius*sqrt(((sin(Theta)-sin(ThetaMistaken))^2) + ((cos(Theta)-cos(ThetaMistaken))^2) );
end
end


%% Varying height, varying bearing, fixed radius (20 m)
if 1

DRadius = 30; % radius of sound
MRadius = 5; % radius of microphone
Height = 20;

ThetaStep = 10; % degree resolution 10 degree
HH = 0:Height;

NumDeg = 180/ThetaStep;
DiffLoc = zeros(NumDeg+1, Height+1); % height resolution: 1m

for dd = 0:NumDeg
    Theta = dd*ThetaStep/180*pi;
    % Original time difference
    TimeDiff = sqrt((MRadius+DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2)-sqrt((MRadius-DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2);

    TimeDiff3D = sqrt((MRadius+DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2+HH.^2)-sqrt((MRadius-DRadius*cos(Theta))^2+(DRadius*sin(Theta))^2+HH.^2);
    % time diff for the height H
    %TimeDiffHH = TimeDiff3D(1,end); <<== make it vary

    % inverse process: having time delay, assume 2D, find the location (incorrect one)
    tt0 = 0:0.1:180; % 1801 points
    Theta1 = pi*(tt0/180);
    TimeDiff2D = sqrt((MRadius+DRadius*cos(Theta1)).^2+(DRadius*sin(Theta1)).^2)-sqrt((MRadius-DRadius*cos(Theta1)).^2+(DRadius*sin(Theta1)).^2);

    for tt = 1:length(TimeDiff3D)
        [ValDeg, IndDeg] = min(abs(TimeDiff2D-TimeDiff3D(1,tt)));
        DegMistaken = (IndDeg-1)*0.1;
        ThetaMistaken = DegMistaken/180*pi; 
        % Location difference
        DiffLoc(dd+1,tt) = DRadius*sqrt(((sin(Theta)-sin(ThetaMistaken))^2) + ((cos(Theta)-cos(ThetaMistaken))^2) );
    end
end
figure; imagesc(0:Height, 0:ThetaStep:180,DiffLoc); colorbar
title('Localization Error (m) from without 3D (Distance of Sound: 30m)')
ylabel('Angle of Sound Source (degree)');
xlabel('Height (m)');

end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dog house
if 0
% As DRadius = 10m, 45deg ==> 55.5 deg
Psi1 = 55.50/180*pi;
Psi2 = 45/180*pi;
DiffLoc = DRadius*sqrt(((sin(Psi1)-sin(Psi2))^2) + ((cos(Psi1)-cos(Psi2))^2) );
%1.83m error
end
