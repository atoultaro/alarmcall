%% NSF Alarm Call Project
% GPS clock decoding

% V1: automatic paging

% Apr 23, 2015
% FINISHED V0: working for a single sound file; no automatic paging yet; beaware that the sound file could be too large to
% fit into memory

% Author: Yu Shiu
% Apr 22, 2015

clear;

FlagPlot = 0;
SoundTarget = 'C:\ASE_Data\__AlarmCall\0407_202741_1.WAV';

% Get sampling rate
SoundInfo = audioinfo(SoundTarget);
Fs = SoundInfo.SampleRate;
% SoundInfo.TotalSamples
% SoundInfo.Duration => 5.1677e+03 sec

%Mess = cell(size(PulseExact,1),1);
%Mess = zeros(size(PulseExact,1),7);
Mess = [];

PageSize = 20*60; % 20 min
LastPage = floor(SoundInfo.Duration/PageSize);

PulseExactCount = 0;
for pp = 1:LastPage

    fprintf('Time: %3.2f hour(s)\n', (pp-1)*PageSize/60/60);
    % Get sound samples
    %[Samples, Fs2] = audioread(SoundTarget, [120*Fs+1, 20*60*Fs]);
    %[Samples, Fs2] = audioread(SoundTarget, [120*Fs+1, 40*60*Fs]);
    if(pp ~= LastPage)
        [Samples, Fs2] = audioread(SoundTarget, [(pp-1)*PageSize*Fs+1, pp*PageSize*Fs]);
    elseif(SoundInfo.Duration == LastPage*PageSize) % The last page is too small; skip!
        continue; 
    else
        [Samples, Fs2] = audioread(SoundTarget, [(pp-1)*PageSize*Fs+1, SoundInfo.Duration*Fs]);
    end


    % Look at channel 1 only
    Samples1 = Samples(:,1);
    clear Samples;

    if FlagPlot
    % Show the GPS waveform
        figure(1); plot((1:size(Samples1,1))/Fs, Samples1); grid; title('Samples');
    end

    % 
    Onset = [(diff(Samples1)>0);0].*(Samples1>0).*Samples1;
    if FlagPlot
        figure(2); plot((1:size(Samples1,1))/Fs,Onset); grid; title('Onset');
    end
    OnsetOn = find(Onset>0.6);

    % Find the exact pulse for OnsetOn: "PulseExact"
    PulseExact = [];
    PulseCurr = -100*Fs; % unit: samples
    PulseLargeThre = 28*Fs; % almost 30 sec
    for ii = 1:size(OnsetOn,1)
       if( OnsetOn(ii,1)-PulseCurr > PulseLargeThre )
           PulseCurr = OnsetOn(ii,1);
           PulseExact = [PulseExact; PulseCurr];
       end
    end

    % display the between-pulse interval
    IOI = diff(PulseExact)/Fs;
    disp(IOI)

    % One data train
    SigThre = 0.1;
    Mess = [Mess; zeros(size(PulseExact,1),7)];
    for jj = 1:size(PulseExact,1)
        %Samples2 = Samples1(OnsetOn(jj,1)+0.150*Fs:OnsetOn(jj,1)+0.250*Fs,1); % between 150 msec and 250 sec after the sync pulse
        Samples2 = Samples1(PulseExact(jj,1)+0.150*Fs:PulseExact(jj,1)+0.250*Fs,1); % between 150 msec and 250 sec after the sync pulse
        Sample2Diff = diff(Samples2);

        % Find zero-crossing point
        SamplesZC = Samples2(1:end-1,1).*Samples2(2:end,1);
        %%SamplesZC2 = (SamplesZC<0).*abs(diff(Samples2));
        %SamplesZC2 = (SamplesZC<0).*(abs(diff(Samples2))>SigThre).*sign(diff(Samples2)); % 0.1 is the threshold for level change
        SamplesZC2 = (SamplesZC<0).*(abs(diff(Samples2))>0.05).*sign(diff(Samples2)); % 0.1 is the threshold for level change
        %figure; plot(SamplesZC2,'-o'); grid; title('ZC2')

        % Find the double-pulses
        SyncPulse = [];
        SamplesZCPulse = find(abs(SamplesZC2) == 1);
        for nn = 1:size(SamplesZCPulse)-3+1
            if( SamplesZC2(SamplesZCPulse(nn,1),1)==-1 &&  SamplesZC2(SamplesZCPulse(nn+1,1),1)==1 && SamplesZC2(SamplesZCPulse(nn+2,1),1)==-1 && SamplesZCPulse(nn+2,1)-SamplesZCPulse(nn,1)<1e-3*Fs)
                %disp(SamplesZCPulse(nn+2,1));
                SyncPulse = [SyncPulse; SamplesZCPulse(nn+2,1)];
            end
        end
        if(length(SyncPulse) ~= 8)
            disp('ERROR: the message contains message of incorrect length.');
            continue
        end

        %mm=1; Samples2(SyncPulse(mm,1)+1:48:SyncPulse(mm,1)+48*8,1)
        MessInBit = zeros(7,8);
        BitDur = 1e-3*Fs; % sample length for each bit
        for kk = 1:7
            for ll = 1:8
                %MessTemp(kk,ll) = Samples2( SyncPulse(kk,1)+BitDur*(ll-1)+1:SyncPulse(kk,1)+BitDur*ll ,1);
                %disp(Samples2( SyncPulse(kk,1)+BitDur*(ll-1)+1+12:SyncPulse(kk,1)+BitDur*ll+12 ,1));
                %fprintf('START: %d, END: %d\n', SyncPulse(kk,1)+BitDur*(ll-1)+1, SyncPulse(kk,1)+BitDur*ll);
                MessInBit(kk,ll) = mode(sign(Samples2( SyncPulse(kk,1)+BitDur*(ll-1)+1+BitDur/4:SyncPulse(kk,1)+BitDur*ll+BitDur/4 ,1)));
            end
        end
        MessInBit = MessInBit/2.0+0.5;
        for kk = 1:7
            MessTemp = 0;
            for ll = 1:8
                %Mess(jj,kk) = Mess(jj,kk)+ MessInBit(kk,ll)*2^(8-ll);
                MessTemp = MessTemp+ MessInBit(kk,ll)*2^(8-ll);
                Mess(PulseExactCount+jj,kk) = MessTemp;
            end
        end
        disp(Mess(PulseExactCount+jj,:));
    end
    PulseExactCount = PulseExactCount + size(PulseExact,1);
    
    %% Write out to a selection table
    % Events are the sync-pulses
    
    
    
end % pp

