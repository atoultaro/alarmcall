function [SamplesOut, Fs] = TimeAlignStretchSeg(SoundMonoFolder, SoundList, PulseLocList, Param)

% Time alignment 
% Time Stretching
% Using all the time points

NumSoundFile = size(SoundList,1);

% Start time: T0; End time: T1
AbsoTime = cell(NumSoundFile,1);
T0 = zeros(NumSoundFile,1);
T1 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    %AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60 + PulseLocList{ii,1}(:,3);
    %T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60 + PulseLocList{ii,1}(1,3);
    %T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60 + PulseLocList{ii,1}(end,3);
    AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60;
    T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60;
    T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60;
end

[MaxT0, MaxT0Ind] = max(T0);
SyncInstT0 = zeros(NumSoundFile,1);
IndT0 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    IndT0(ii,1) = find(abs(AbsoTime{ii,1}-MaxT0)==0);
    if(ii ~= MaxT0Ind)
        try
            %SyncInstT0(ii,1) = PulseLocList{ii,1}(IndT0(ii,1),9);
            SyncInstT0(ii,1) = PulseLocList{ii,1}(IndT0(ii,1),11); % Synchronizer hardware must've been changed between late November and Feb 3.
        catch
            disp('');
        end
   else
       %SyncInstT0(ii,1) = PulseLocList{ii,1}(1,9);
       SyncInstT0(ii,1) = PulseLocList{ii,1}(1,11);
   end
end

% End time
%[fid,msg] = fopen(fullfile(SoundMonoFolder, SoundList(ii,1).name),'rb','l');
[MinT1, MinT1Ind] = min(T1);

PulseNum = (MinT1 - MaxT0)/Param.SecBetPulse;
PulseTable = zeros(PulseNum, NumSoundFile);
for ii = 1:NumSoundFile
    for jj = 1:PulseNum
        PulseTable(jj, ii) = PulseLocList{ii,1}(IndT0(ii,1)+jj-1,11); % IndT0(ii,1)
    end
end

SampleDropped = Param.Fs*Param.SecBetPulse - (diff(PulseTable) + 1); % they were dropped during recording process and hence should be added

disp('');

%SamplesOut = zeros((PulseNum-1)*Param.Fs*Param.SecBetPulse ,NumSoundFile,'int16');
SamplesOut = zeros((PulseNum-1)*Param.Fs*Param.SecBetPulse ,NumSoundFile);
for ii = 1:NumSoundFile
    NumSeg = size(SampleDropped,1);
    for jj = 1:NumSeg
        %SamplesOutTemp = [];
        fprintf('%d of %d\n', jj, NumSeg);
        %[SamplesBothChanTemp, Fs] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'native');
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)]);
        SamplesInTemp = SamplesBothChanTemp(:,1); % Channel 1 is the data channel whereas Channel 2 is the sync channel
        clear SamplesBothChanTemp; 
        
        
        if(SampleDropped(jj,ii) > 0)
            % insert samples
            SamplesOutTemp = SamplesInsert(SamplesInTemp, SampleDropped(jj,ii));
        elseif(SampleDropped(jj,ii) <0)
            % drop samples
            SamplesOutTemp = SamplesRemove(SamplesInTemp, -1*SampleDropped(jj,ii));
        else % SampleDropped(jj,ii) == 0
            % do nothing and out the original samples
        end
        
        try
            SamplesOut((jj-1)*Param.Fs*Param.SecBetPulse+1:jj*Param.Fs*Param.SecBetPulse,ii) = SamplesOutTemp;
        catch
            disp();
        end
    end
    disp('');
end

function SamplesOutput = SamplesInsert(SamplesInput, SamplesToPut)
SamplesOutput = [];
% How to insert samples?
% * uniform samples the indice and record them
% * read over all samples and insert accordingly
%SamplesInsertLen = floor(Param.Fs*Param.SecBetPulse/(SamplesToPut+1)); %% <<== problem!!
SamplesInsertLen = floor(length(SamplesInput)/(SamplesToPut+1));
if((ceil(length(SamplesInput)/SamplesInsertLen)-1)-1 ~= SamplesToPut)
    fprintf('');
end

for pp = 1:SamplesToPut
    SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1); SamplesInput(pp*SamplesInsertLen,1)];
end
pp = pp+1;
SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesInsertLen+1:end,1)];


function SamplesOutput = SamplesRemove(SamplesInput, SamplesToGet)
SamplesOutput = [];
SamplesRemoveLen = floor(length(SamplesInput)/(SamplesToGet+1));

for pp = 1:SamplesToGet
    %SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1); SamplesInput(pp*SamplesInsertLen,1)];
    SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesRemoveLen+1:pp*SamplesRemoveLen-1,1)]; % remove one sample at the end of each segment
end
pp = pp+1;
SamplesOutput = [SamplesOutput; SamplesInput((pp-1)*SamplesRemoveLen+1:end,1)];
















