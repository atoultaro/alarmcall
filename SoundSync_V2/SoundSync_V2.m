
% Dec 23, 2015
% Version 2 is done!
% Memory limitation is relaxed. Good for computers with 8GB for most scenarios (96kHz, half an hour, 8 channels).
% Good for 2-channel recorder on each node.
% To-do: 1. Include 4-channel nodes
% 2. Work for missing detection of large pulses
% 3. Detect GPS channels automatically


% Nov 30, 2015
% Author: Yu Shiu
% Goal: sync-and-merge 4 sources of high-quality sound data
% Assumptions: 1-min large pulses can be reliably detected. No missing pulses.

% The sound came from 4 recorders, each of which records stereo: one for sound and one for synchronizer. that is, 8
% channels of sounds are present.
% High-quality sound data: 48kHz sampling rate, 24-bit samples

% Memory concern:
% 2 hours, 48k samplerate, 2 channels will need 5.8 GByte of memory
% Or, 1 hour, 96kHz, 2 channels.
% 30min, 96kHz, 2 channels needs 2.8 GByte, might be pass-able for a computer with 8 GByte.

clear;
%% Parameters
% SecBetPulse = 60; % the interval between two adjacent pulses
GPSChan = 2; % Channel for synchronizer
NonGPSChan = 1;
OnsetThre = 0.6; % Threshold to detect the large pulse

%Param.Fs = 48000; % Sampling rate
Param.SecBetPulse = 60; % second between two adjacent large pulses
Param.GPSChan = GPSChan;
Param.NonGPSChan = NonGPSChan;

%% Sound input folder & output single multi-channel file
SoundInputFolder = 'C:\ASE_Data\__AlarmCall\__20151129Pat';
SoundOutputFolder = 'C:\ASE_Data\__AlarmCall';
OutputFilenameBase = 'SyncAndMerge';

%% read sound list
SoundList = dir(fullfile(SoundInputFolder, '*.aif'));
if(size(SoundList,1)==0)
    SoundList = dir(fullfile(SoundInputFolder, '*.wav'));
end

%% read sample rate & bit per sample
SoundNum = size(SoundList,1);
SampleRate = zeros(SoundNum,1);
BitPerSample = zeros(SoundNum,1);
for cc = 1:SoundNum
    Info = audioinfo(fullfile(SoundInputFolder, SoundList(cc,1).name));
    SampleRate(cc,1) = Info.SampleRate;
    BitPerSample(cc,1) = Info.BitsPerSample;
end

%% Check sample rate & bit per sample before do the job
if(length(find(SampleRate-mean(SampleRate,1)~= 0)) || length(find(BitPerSample-mean(BitPerSample,1)~= 0)) )
    disp('Either sampling rates or bit-per-sample do not match among all the channels.');
else
    Param.Fs = Info.SampleRate;
    % Do thing here
    %% Retrieve the data payload of large pulses
    disp('Retrieve time codes......')
    PulseLocList = GetLargePulse(SoundInputFolder, SoundList, GPSChan, OnsetThre);

    %% Time stretch & align
    % time sliding calculation
    % time stretching calculation
    % Generate the synced & stretched sounds
    %[SamplesOut, Fs] = TimeAlignStretch(SoundMonoFolder, SoundList, PulseLocList); % create new sound files under a folder in SoundMonoFolder
    %[SamplesOut, Fs] = TimeAlignStretchSeg(SoundInputFolder, SoundList, PulseLocList, Param); 
    % Write wav output
    %audiowrite(fullfile(SoundOutputFolder,OutputFilename), SamplesOut,Fs);
    
    disp('Sync and merge sound files......');
    % combine time-alignment, merge and sound files write out
    TimeAlignStretchSeg(SoundInputFolder, SoundList, PulseLocList, SoundOutputFolder, OutputFilenameBase, Param); 
end


