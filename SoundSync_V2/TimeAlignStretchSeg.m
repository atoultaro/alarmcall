%function [SamplesOut, Fs] = TimeAlignStretchSeg(SoundInputFolder, SoundList, PulseLocList, Param)
function [SamplesOut, Fs] = TimeAlignStretchSeg(SoundInputFolder, SoundList, PulseLocList, SoundOutputFolder, OutputFilenameBase, Param)

% Time alignment 
% Time Stretching
% Using all the time points

% TO-DO:
% 1. should output sound files in many parts, in order to save the memory usuage; use "sox" to concatenate/combine
% 2. merging using sparse large pulse indices
% 3. Support 4-channel roland

%Param.NonGPSChan = 1;

NumSoundIn = size(SoundList,1);

% Start time: T0; End time: T1
AbsoTime = cell(NumSoundIn,1);
T0 = zeros(NumSoundIn,1);
T1 = zeros(NumSoundIn,1);
for ii = 1:NumSoundIn
    %% seconds == column 3 
    %AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60 + PulseLocList{ii,1}(:,3);
    %T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60 + PulseLocList{ii,1}(1,3);
    %T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60 + PulseLocList{ii,1}(end,3);
    %% seconds == 0.0
    AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60 + 0.0;
    T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60 + 0.0;
    T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60 + 0.0;
end

[MaxT0, MaxT0Ind] = max(T0);
SyncInstT0 = zeros(NumSoundIn,1);
IndT0 = zeros(NumSoundIn,1);
for ii = 1:NumSoundIn
    IndT0(ii,1) = find(abs(AbsoTime{ii,1}-MaxT0)==0);
    if(ii ~= MaxT0Ind)
        try
            SyncInstT0(ii,1) = PulseLocList{ii,1}(IndT0(ii,1),end);
        catch
            disp('');
        end
   else
       SyncInstT0(ii,1) = PulseLocList{ii,1}(1,end);
   end
end

% End time
%[fid,msg] = fopen(fullfile(SoundInputFolder, SoundList(ii,1).name),'rb','l');
[MinT1, ~] = min(T1);

PulseNum = (MinT1 - MaxT0)/Param.SecBetPulse;
PulseTable = zeros(PulseNum, NumSoundIn);
for ii = 1:NumSoundIn
    for jj = 1:PulseNum
        PulseTable(jj, ii) = PulseLocList{ii,1}(IndT0(ii,1)+jj-1,end); % IndT0(ii,1)
    end
end

%SampleDropped = Param.Fs*Param.SecBetPulse - (diff(PulseTable) + 1); % they were dropped during recording process and hence should be added
SampleDropped = Param.Fs*Param.SecBetPulse - diff(PulseTable);

if(any(SampleDropped(:)<0))
    disp('Samples are more than it is supposed to be. Number of samples is larger than Samplerate x 60 sec.');
    return;
end
disp('');

%% Insert samples and merge
% To-Do: should output sound files in many parts, in order to save the memory usuage; use "sox" to concatenate/combine
% sound files
LengthSoundOut = 15; % the length of output sound files ==>> 15 times of Large Pulse Period => 15x60sec => 15 min
NumSoundOut = floor((PulseNum-1)/LengthSoundOut)+1;

FileIndex = 0;
for nn = 1:NumSoundOut
    if(nn ~= NumSoundOut)
        %SamplesOut = TimeAlign(SoundInputFolder, SoundList(ii,1).name , PulseTablePartial, SampleDroppedPartial, LengthSoundOut);
        SamplesOut = TimeAlign(SoundInputFolder, SoundList(ii,1).name , PulseTable((nn-1)*LengthSoundOut+1:nn*LengthSoundOut+1,:), SampleDropped((nn-1)*LengthSoundOut+1:nn*LengthSoundOut,:), LengthSoundOut, NumSoundIn, Param);
        FileIndex = FileIndex + 1;
        audiowrite(fullfile(SoundOutputFolder, [OutputFilenameBase, num2str(FileIndex),'.wav']), SamplesOut, Param.Fs);
        
    else % nn == NumSoundOut
        SamplesOut = TimeAlign(SoundInputFolder, SoundList(ii,1).name , PulseTable((nn-1)*LengthSoundOut+1:end,:), SampleDropped((nn-1)*LengthSoundOut+1:end,:), LengthSoundOut, NumSoundIn, Param);
        FileIndex = FileIndex + 1;
        audiowrite(fullfile(SoundOutputFolder, [OutputFilenameBase, num2str(FileIndex),'.wav']), SamplesOut, Param.Fs);
    end
    
end

function SamplesOut = TimeAlign(SoundInputFolder, SoundListName, PulseTablePartial, SampleDroppedPartial, LengthSoundOut0, NumSoundIn, Param) % SoundList(ii,1).name; 
LengthSoundOut = min(LengthSoundOut0, size(SampleDroppedPartial,1));
SamplesOut = zeros(LengthSoundOut*Param.Fs*Param.SecBetPulse , NumSoundIn, 'double');
for ii = 1:NumSoundIn
    SamplesOutTemp = [];
    for jj = 1:LengthSoundOut
        fprintf('%d of %d\n', jj, LengthSoundOut);
        %[SamplesBothChanTemp, Fs] = audioread(fullfile(SoundInputFolder, SoundListName), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'native');
        try
            [SamplesInTemp, Fs] = audioread(fullfile(SoundInputFolder, SoundListName), [PulseTablePartial(jj,ii), PulseTablePartial(jj+1,ii)-1], 'double');
        catch
            disp('') % debugs
        end
        SamplesInTemp = SamplesInTemp(:, Param.NonGPSChan); 

        SamplesInsertLen = floor(length(SamplesInTemp)/(SampleDroppedPartial(jj,ii)+1));
        if((ceil(length(SamplesInTemp)/SamplesInsertLen)-1)-1 ~= SampleDroppedPartial(jj,ii))
            fprintf('');
        end

        % insert samples
        for pp = 1:SampleDroppedPartial(jj,ii)
            SamplesOutTemp = [SamplesOutTemp; SamplesInTemp((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1); SamplesInTemp(pp*SamplesInsertLen,1)];
        end
        pp = pp+1;
        %SamplesOutTemp = [SamplesOutTemp; SamplesInTemp((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1)];
        SamplesOutTemp = [SamplesOutTemp; SamplesInTemp((pp-1)*SamplesInsertLen+1:end,1)];
        disp('');
    end
    SamplesOut(:,ii) = SamplesOutTemp;
end
return






if 0
SamplesOut = zeros((PulseNum-1)*Param.Fs*Param.SecBetPulse ,NumSoundIn,'int16'); % only works on 16-bit samples'; need to be generalized to 24-bit!
for ii = 1:NumSoundIn
    SamplesOutTemp = [];
    for jj = 1:PulseNum
        fprintf('%d of %d\n', jj, PulseNum);
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundInputFolder, SoundList(ii,1).name), [PulseTable(jj,ii), PulseTable(jj+1,ii)], 'native');
        SamplesInTemp = SamplesBothChanTemp(:, Param.GPSChan);
        clear SamplesBothChanTemp; 
        %SamplesOutTemp = zeros(RefNumSamp,1,'int16');
        % Add samples (SampleDropped(jj,ii)) into  SamplesInTemp
        
        % How to insert samples?
        % * uniform sampling the indice and record them
        % * read over all samples and insert accordingly
        %SamplesInsertLen = floor(Param.Fs*Param.SecBetPulse/(SampleDropped(jj,ii)+1)); %% <<== problem!!
        SamplesInsertLen = floor(length(SamplesInTemp)/(SampleDropped(jj,ii)+1));
        if((ceil(length(SamplesInTemp)/SamplesInsertLen)-1)-1 ~= SampleDropped(jj,ii))
            fprintf('');
        end
        %for pp = 1:(ceil(length(SamplesInTemp)/SamplesInsertLen)-1)-1 %% <<== problem!!
        for pp = 1:SampleDropped(jj,ii)
            SamplesOutTemp = [SamplesOutTemp; SamplesInTemp((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1); SamplesInTemp(pp*SamplesInsertLen,1)];
        end
        pp = pp+1;
        %SamplesOutTemp = [SamplesOutTemp; SamplesInTemp((pp-1)*SamplesInsertLen+1:pp*SamplesInsertLen,1)];
        SamplesOutTemp = [SamplesOutTemp; SamplesInTemp((pp-1)*SamplesInsertLen+1:end,1)];
        
        disp('');
    end
    SamplesOut(:,ii) = SamplesOutTemp;
    disp('');
end
end








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Dog house %%
if 0
SyncInstT1 = zeros(NumSoundIn,1);
for ii = 1:NumSoundIn
   if(ii ~= MinT1Ind)
       SyncInstT1(ii,1) = PulseLocList{ii,1}(find(abs(AbsoTime{ii,1}-MinT1)==0 ),9);
   else
       SyncInstT1(ii,1) = PulseLocList{ii,1}(end,9);
   end
end

TDura0 = MinT1 - MaxT0; % sec


SamplesDuraObs = SyncInstT1 - SyncInstT0;

% Assuming "clock delay": less samples than supposed to be

% Method 1: using the shortest SamplesDuraObs as reference
[RefNumSamp, RefChanInd] = min(SamplesDuraObs);
RemoveNumSamp = SamplesDuraObs - RefNumSamp;

% Remove samples / stretching samples and write to sound files
disp('');
%SamplesOut = zeros(RefNumSamp, NumSoundIn);
SamplesOut = zeros(RefNumSamp, NumSoundIn,'int16');
for ii = 1:NumSoundIn
    %SoundTarget = sound_create('file', fullfile(SoundInputFolder, SoundList(ii,1).name) );
    if(ii ~= RefChanInd)
        %SamplesInTemp = sound_read(SoundTarget, 'samples', SyncInstT0(ii,1),SamplesDuraObs(ii,1) ,1);
        %[SamplesInTemp, ~] = audioread(fullfile(SoundInputFolder, SoundList(ii,1).name), [SyncInstT0(ii,1), SyncInstT0(ii,1)+SamplesDuraObs(ii,1)-1], 'int16');
        %SamplesBothChanTemp = [];
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundInputFolder, SoundList(ii,1).name), [SyncInstT0(ii,1), SyncInstT0(ii,1)+SamplesDuraObs(ii,1)-1], 'native');
        SamplesInTemp = SamplesBothChanTemp(:,1);
        SamplesBothChanTemp = [];
        
        SamplesOutTemp = zeros(RefNumSamp,1,'int16');
        
        % remove samples
        AntiDecimate = round(size(SamplesOutTemp,1)/RemoveNumSamp(ii,1) );
        for ss = 1:RemoveNumSamp(ii,1)
            SamplesOutTemp((ss-1)*(AntiDecimate-1)+1:ss*(AntiDecimate-1),1) = SamplesInTemp((ss-1)*AntiDecimate+1:ss*AntiDecimate-1,1); % -1 means lose one sample at a time
            disp('');
        end
        SamplesOutTemp(RemoveNumSamp(ii,1)*(AntiDecimate-1)+1:end,1) = SamplesInTemp(RemoveNumSamp(ii,1)*AntiDecimate+1:end,1);
        SamplesInTemp = [];
        
        % return SamplesOut{ii,1}
        SamplesOut(:,ii) = SamplesOutTemp;
        SamplesOutTemp = [];
    else
        %SamplesOut(:,ii) = sound_read(SoundTarget, 'samples', SyncInstT0(ii,1),SamplesDuraObs(ii,1),1);
        %SamplesBothChanTemp = [];
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundInputFolder, SoundList(ii,1).name), [SyncInstT0(ii,1), SyncInstT0(ii,1)+SamplesDuraObs(ii,1)-1], 'native');
        SamplesOut(:,ii) = SamplesBothChanTemp(:,1); % use only chan 1; chan 2 is the gps clock channel
        SamplesBothChanTemp = [];
        disp('');
    end
end

end