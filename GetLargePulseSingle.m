
function PulseLocList = GetLargePulseSingle(SoundFile)

if 0
% Example
%SoundFile = 'C:\ASE_Data\__AlarmCall\__ChannelCombine\NSFAlarmCalls_48k_gain_test_R-26 tracks_20150610_143602.wav';
SoundFile = 'C:\ASE_Data\__AlarmCall\__20151129PatShort\1129_175521_1_Short.wav'
PulseLocList = GetLargePulseSingle(SoundFile)
end

GPSChan = 2;
OnsetThre = 0.6;
[Samples, Fs] = audioread(SoundFile, 'double');
Samples = Samples(:,GPSChan);
PulseLocList = GetLargePulsePerSeg(Samples, Fs, OnsetThre);
disp('')



%{
GPSChan = 2;
PulseLocList = cell(size(SoundList,1),1);
for ii = 1:size(SoundList,1)
    disp(fullfile(SoundMonoFolder, SoundList(ii,1).name))
    %SoundTarget = sound_create('file', fullfile(SoundMonoFolder, SoundList(ii,1).name) );
    %SamplesTarget = sound_read(SoundTarget);
    [SamplesTarget, Fs] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), 'double');
    
    %PulseLocList{ii,1} = GetLargePulsePerSeg(SamplesTarget(:,GPSChan), SoundTarget.samplerate);
    PulseLocList{ii,1} = GetLargePulsePerSeg(SamplesTarget(:,GPSChan), Fs);
end

%return PulseLocList;
%}
