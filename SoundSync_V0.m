
% May 7, 2015
% Author: Yu Shiu

clear;
% Samplerate
%Fs = 48000;
% Sound folder
%SoundMonoFolder = 'C:\ASE_Data\__AlarmCall\__ChannelCombine\dual coincident quad arrays May 6th 2015';
SoundMonoFolder = 'C:\ASE_Data\__AlarmCall\__ChannelCombine\quad sync drift test May 7th';
OutputFilename = 'SyncAllChan.wav';

%%
SoundList = dir(fullfile(SoundMonoFolder, '*.wav'));

% Get the data payload of large pulses and its sample number / timing
PulseLocList = GetLargePulse(SoundMonoFolder, SoundList);



% time sliding calculation
% time stretching calculation
% Generate the synced & stretched sounds
[SamplesOut, Fs] = TimeAlignStretch(SoundMonoFolder, SoundList, PulseLocList); % create new sound files under a folder in SoundMonoFolder

% Write wav output
audiowrite(fullfile(SoundMonoFolder,OutputFilename), SamplesOut,Fs);



