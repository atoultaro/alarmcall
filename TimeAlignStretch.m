function [SamplesOut, Fs] = TimeAlignStretch(SoundMonoFolder, SoundList, PulseLocList)

% Time alignment 
% Time Stretching
% Using the first and the last time point

NumSoundFile = size(SoundList,1);

% Start time: T0; End time: T1
AbsoTime = cell(NumSoundFile,1);
T0 = zeros(NumSoundFile,1);
T1 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    AbsoTime{ii,1} = PulseLocList{ii,1}(:,1)*60*60 + PulseLocList{ii,1}(:,2)*60 + PulseLocList{ii,1}(:,3);
    T0(ii,1) = PulseLocList{ii,1}(1,1)*60*60 + PulseLocList{ii,1}(1,2)*60 + PulseLocList{ii,1}(1,3);
    T1(ii,1) = PulseLocList{ii,1}(end,1)*60*60 + PulseLocList{ii,1}(end,2)*60 + PulseLocList{ii,1}(end,3);
end

[MaxT0, MaxT0Ind] = max(T0);
SyncInstT0 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
    if(ii ~= MaxT0Ind)
        try
            SyncInstT0(ii,1) = PulseLocList{ii,1}(find(abs(AbsoTime{ii,1}-MaxT0)==0 ),9);
        catch
            disp('');
        end
   else
       SyncInstT0(ii,1) = PulseLocList{ii,1}(1,9);
   end
end

% End time


%[fid,msg] = fopen(fullfile(SoundMonoFolder, SoundList(ii,1).name),'rb','l');
[MinT1, MinT1Ind] = min(T1);
SyncInstT1 = zeros(NumSoundFile,1);
for ii = 1:NumSoundFile
   if(ii ~= MinT1Ind)
       SyncInstT1(ii,1) = PulseLocList{ii,1}(find(abs(AbsoTime{ii,1}-MinT1)==0 ),9);
   else
       SyncInstT1(ii,1) = PulseLocList{ii,1}(end,9);
   end
end

TDura0 = MinT1 - MaxT0; % sec


SamplesDuraObs = SyncInstT1 - SyncInstT0;

% Assuming "clock delay": less samples than supposed to be

% Method 1: using the shortest SamplesDuraObs as reference
[RefNumSamp, RefChanInd] = min(SamplesDuraObs);
RemoveNumSamp = SamplesDuraObs - RefNumSamp;

% Remove samples / stretching samples and write to sound files
disp('');
%SamplesOut = zeros(RefNumSamp, NumSoundFile);
SamplesOut = zeros(RefNumSamp, NumSoundFile,'int16');
for ii = 1:NumSoundFile
    %SoundTarget = sound_create('file', fullfile(SoundMonoFolder, SoundList(ii,1).name) );
    if(ii ~= RefChanInd)
        %SamplesInTemp = sound_read(SoundTarget, 'samples', SyncInstT0(ii,1),SamplesDuraObs(ii,1) ,1);
        %[SamplesInTemp, ~] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), [SyncInstT0(ii,1), SyncInstT0(ii,1)+SamplesDuraObs(ii,1)-1], 'int16');
        %SamplesBothChanTemp = [];
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), [SyncInstT0(ii,1), SyncInstT0(ii,1)+SamplesDuraObs(ii,1)-1], 'native');
        SamplesInTemp = SamplesBothChanTemp(:,1);
        SamplesBothChanTemp = [];
        
        SamplesOutTemp = zeros(RefNumSamp,1,'int16');
        
        % remove samples
        AntiDecimate = round(size(SamplesOutTemp,1)/RemoveNumSamp(ii,1) );
        for ss = 1:RemoveNumSamp(ii,1)
            SamplesOutTemp((ss-1)*(AntiDecimate-1)+1:ss*(AntiDecimate-1),1) = SamplesInTemp((ss-1)*AntiDecimate+1:ss*AntiDecimate-1,1); % -1 means lose one sample at a time
            disp('');
        end
        SamplesOutTemp(RemoveNumSamp(ii,1)*(AntiDecimate-1)+1:end,1) = SamplesInTemp(RemoveNumSamp(ii,1)*AntiDecimate+1:end,1);
        SamplesInTemp = [];
        
        % return SamplesOut{ii,1}
        SamplesOut(:,ii) = SamplesOutTemp;
        SamplesOutTemp = [];
    else
        %SamplesOut(:,ii) = sound_read(SoundTarget, 'samples', SyncInstT0(ii,1),SamplesDuraObs(ii,1),1);
        %SamplesBothChanTemp = [];
        [SamplesBothChanTemp, Fs] = audioread(fullfile(SoundMonoFolder, SoundList(ii,1).name), [SyncInstT0(ii,1), SyncInstT0(ii,1)+SamplesDuraObs(ii,1)-1], 'native');
        SamplesOut(:,ii) = SamplesBothChanTemp(:,1); % use only chan 1; chan 2 is the gps clock channel
        SamplesBothChanTemp = [];
        disp('');
    end
end


% Method 2: 
%SamplesDura0 = TDura0*Fs;
%SamplesStretch = SamplesDuraObs - SamplesDura0;



disp('');
